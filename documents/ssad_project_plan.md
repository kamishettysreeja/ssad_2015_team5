
#  SSAD 


####Team number 	----	 Team 5


----------


####Project Title   		----- Saving the world- One shopping bag at time


----------


####Document 		-----	 Project Plan


----------


####Creation date 		-----  28th august 2015


----------


####Created By			-----   Anjali shenoy ,A Ramya Keerthana, Dhruv Sapra,kamishetty sreeja and Saumya Rawat


----------


####Client  ------     				Sandra Riedel



----------
###Brief problem statement

The world is in grave danger because of overuse of plastic. Hence to address 
this issue, Polar Bear International in collaboration with team 5 IIITH SERC has 
come up with the idea to develop a fun, And interactive game to educate its 
users on the harmful effects of use of plastic.

The game will be a simulation of the daily lives set in an urban neighbourhood, where different Levels will make the players face different consumer choices. The more green your decisions, the Lesser “plastic points” you gather, the more closer you are to unlocking higher levels.

1.The game idea involves a moving trolley driven by the user using their phone controls (tilt+swipe) The trolley is initially between shelves in a super market. As they start moving, items off the shelf Will keep trying to enter the trolley. The user has to avoid these by tilting the trolley away from the Non-eco friendly items, or by swiping them off the screen. 

The time limit is set by the user. As time Prolongs, the speed of the falling items increases and even their frequency. If at any point in the Game, the user crosses the maximum limit of “-ve” items, a warning is showed. At the end, the User With the most number of eco-friendly products in their trolley wins.

####2.Game Progression
The 1st Scene is the player gets option to choose
#####-Play game – Starts a New Game
#####-High Score – Shows High score
#####-Settings -- For display ,sound , selection Of Characters
#####-How to Play – Where the game rules and hints to how to play the game is displayed

The player can choose one among OLPM . The player is given 3 lives when player misses to swipe any bio-degradable thing he will loose 1 life or when the player swipes nonbiodegradable thing he looses one life and there will be a timer we the time limit exceeds the game ends .

####Mechanics of the Game
The Player can swipe through the object or touch the object or select the object . We will be having the arrow on the screen for different directions.
###Team Members
####Anjali Shenoy (Artist):
Game artists are responsible for all of the aspects of game development that call for visual art. Artists create the visual elements of a game, such as characters, scenery, objects, vehicles, surface Textures, clothing, props, and even user interface components. This could be in 2D or 3D depending on the specifications.

Game artists generally know how to use 3D graphics packages such as 3D Studio Max, Maya,
etc., and 2D packages such as Photoshop.

####A Ramya Keerthana (Level designer):
Level designer creates the basic and the higher level environment for the game using level editors and other game developer tools.It involves creation of game levels.

Level editors like 3D contruction kit,chromEd,BF2 editor; Graphic editors like Photoshop and IDEs like Microsoft Visual Studio can be used.

####Dhruv Sapra (Sound Engg):

According to me the work of sound engineer starts once the designer and the artist gives a formal shape to the game. The sound engineer is the one who adds that extra pinch of salt in the form of the music which enhances the gaming experience of the user.

 Tunes are something which remain in the minds of the user even after finishing his game.. Some softwares that I have come across which could help me in this job are Adobe After Effect and Sony vegas Pro.

I will need to clearly communicate with the artist and designer regarding the different music tunes they need which could add fun and excitement into the characters.I think we may keep choice of their music to be played
in the background so that more users like it.

####Kamishetty Sreeja (Programmer):

Since it is a cross Platform application for both iOS and android it is better to develop on software which supports it.according to me some of the softwares we can use are. It includes audio programming graphics artificial intelligence programming

1- RAD Studio XE8
2- Unity
3- IBM Worklight
4- Apple Xcode
5- Android SDK

####Saumya Rawat (Designer):

As per my understanding a designer's role in a game development project would be to devise what is the gameplay and define the core elements of the game.The basic structure, objects, props, story flow, modes of play and characters.

I will need to clearly communicate my vision and ideas to the rest of the team. After initial research is complete a game designer puts together the concept document that will contain the intended gameplay and define the game functionality.

As a basic gameplay, the character will start out in a city landscape, the user can choose profiles and various characters. 

The goal of the game is to restrain the bothersome polluters in different parts of the city who sour the life of the citizens with their continuous annoyance. and we have to find clever knacks to change back our residence into the island of peace and greenery. The one with the LOWEST plastic points wins.

###Team Communication:

Team meeting with client and TA at CIE every Wednesday 10am.
Bit bucket account to co-ordinate work

Facebook group for SSAD Team 5 of only team members and TA to ask queries and post doubts.

WhatsApp group with client and TA to quickly pass information regarding meetings or any other general queries

WhatsApp group of only team members to discuss intra team related issues 
Google Hangouts/Skype for discussion
###Development Environment
Environment: Unity

Language: C++(tentative)

collaboration tools: Photoshop, 3D Studio Max, Maya, Adobe after effects, Sony 
Vegas Pro,

ChromeEd, BF2 editor,
Milestone Schedule

###Milestone

This is to be Finalised by next week.



