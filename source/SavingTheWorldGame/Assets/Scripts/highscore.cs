﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class highscore : IComparable<highscore>
{

	public int score { get; set; }
	public string name { get; set; }
	public string city { get; set; }
	public string country { get; set; }
	public string email { get; set; }
	public int ID { get; set; }
	//constructor
	public highscore(int score, string name, string city, string country, string email, int ID)
	{
		this.score = score;
		this.name = name;
		this.city = city;
		this.country = country;
		this.email = email;
		this.ID = ID;
	}
	public int CompareTo(highscore other)
	{
		if (other.score > this.score)
			return 1;
		else if (other.score < this.score)
			return -1;

		return 0;

	}
}
